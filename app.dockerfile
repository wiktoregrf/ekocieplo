FROM php:7.4-fpm-alpine as alpine-xdebug
RUN apk add --update make autoconf gcc g++
RUN wget http://xdebug.org/files/xdebug-2.9.6.tgz && tar -xf xdebug-2.9.6.tgz
WORKDIR /var/www/html/xdebug-2.9.6
RUN phpize && ./configure && make

FROM php:7.4-fpm-alpine

WORKDIR /var/www/

RUN docker-php-ext-install pdo pdo_mysql

COPY --from=alpine-xdebug /var/www/html/xdebug-2.9.6/modules/xdebug.so /usr/local/lib/php/extensions/no-debug-non-zts-20190902/xdebug.so
RUN echo "zend_extension=xdebug.so" > /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.idekey=docker" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_port=9008" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.auto_start=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_connect_back=0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_autostart=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_handler=dbgp" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.host=192.168.2.72" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini


