.PHONY: ssh fix-permissions composer-install run generate-key migrate seed docker-logs build

ssh:
	@docker-compose exec app sh

fix-permissions:
	@chmod -R 777 storage

migrate:
	@docker-compose exec app php artisan migrate

rollback:
	@docker-compose exec app php artisan migrate:rollback

seed:
	@docker-compose exec app php artisan db:seed

composer-install:
	@sh composer install

composer-dump:
	@sh composer dump-autoload

clear-cache:
	@docker-compose exec app php artisan cache:clear

run:
	@docker-compose up -d ;\

logs:
	@docker-compose logs -f || exit 0 ;\

stop:
	@docker-compose stop

build: composer-install fix-permissions run migrate seed logs
