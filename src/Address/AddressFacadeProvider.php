<?php
declare(strict_types=1);

namespace Ekocieplo\Address;

use Ekocieplo\Address\Read\AddressReadService;
use Illuminate\Support\ServiceProvider;

class AddressFacadeProvider extends ServiceProvider
{
    public function register()
    {
        parent::register();
        $this->app->bind(AddressesFacade::class, function () {
            return new AddressesFacade($this->app->get(AddressReadService::class));
        });
    }

    public function provides()
    {
        return [AddressesFacade::class];
    }
}
