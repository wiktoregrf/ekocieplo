<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Infrastructure\Persistence;

use Ekocieplo\Address\Read\Dto\PlannedHeatingDto;
use Ekocieplo\Address\Read\IPlannedHeatingAddressReadRepository;
use Illuminate\Database\Query\Builder;

class PlannedHeatingAddressReadRepository implements IPlannedHeatingAddressReadRepository
{
    private Builder $builder;

    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

    /** @inheritDoc */
    public function findByAddressId(string $addressId): array
    {
        $plannedHeating = $this->builder
            ->newQuery()
            ->from(TableNames::ADDRESSES_PLANNED_HEATING)
            ->leftJoin(TableNames::HEATING, TableNames::ADDRESSES_PLANNED_HEATING . ".heating_id", "=", TableNames::HEATING . ".id")
            ->where("address_id", "=", $addressId)
            ->get();

        return array_map(function ($data) {
            return PlannedHeatingDto::fromArray((array)$data);
        }, $plannedHeating->toArray());
    }
}
