<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Infrastructure\Persistence;

use Ekocieplo\Address\Read\Dto\HeatingDto;
use Ekocieplo\Address\Read\ICurrentHeatingAddressReadRepository;
use Illuminate\Database\Query\Builder;

class CurrentHeatingAddressReadRepository implements ICurrentHeatingAddressReadRepository
{
    private Builder $builder;

    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

    /**
     * @inheritDoc
     */
    public function findByAddressId(string $addressId): array
    {
        $currentHeating = $this->builder
            ->newQuery()
            ->from(TableNames::ADDRESSES_HEATING)
            ->leftJoin(TableNames::HEATING, TableNames::ADDRESSES_HEATING . ".heating_id", "=", TableNames::HEATING . ".id")
            ->where("address_id", "=", $addressId)
            ->get();

        return array_map(function ($data) {
            return HeatingDto::fromArray((array)$data);
        }, $currentHeating->toArray());
    }
}
