<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Infrastructure\Persistence;

final class TableNames
{
    public const ADDRESSES = "addresses";
    public const ADDRESSES_HEATING = "addresses_heating";
    public const ADDRESSES_PLANNED_HEATING = "addresses_planned_heating";
    public const HEATING = "heating";

}
