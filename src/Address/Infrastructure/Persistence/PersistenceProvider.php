<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Infrastructure\Persistence;

use Ekocieplo\Address\Read\ICurrentHeatingAddressReadRepository;
use Ekocieplo\Address\Read\IPlannedHeatingAddressReadRepository;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Ekocieplo\Address\Read\IAddressReadRepository;

class PersistenceProvider extends ServiceProvider implements DeferrableProvider
{
    public function register()
    {
        $this->app->bind(IAddressReadRepository::class, function () {
            return new AddressReadRepository(DB::table(TableNames::ADDRESSES));
        });

        $this->app->bind(ICurrentHeatingAddressReadRepository::class, function () {
            return new CurrentHeatingAddressReadRepository(DB::table(TableNames::ADDRESSES_HEATING));
        });

        $this->app->bind(IPlannedHeatingAddressReadRepository::class, function () {
            return new PlannedHeatingAddressReadRepository(DB::table(TableNames::ADDRESSES_PLANNED_HEATING));
        });
    }

    public function provides(): array
    {
        return [
            IAddressReadRepository::class,
            ICurrentHeatingAddressReadRepository::class,
            IPlannedHeatingAddressReadRepository::class
        ];
    }
}
