<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Infrastructure\Persistence;

use Ekocieplo\Address\Read\Dto\AddressDto;
use Ekocieplo\Address\Read\IAddressReadRepository;
use Illuminate\Database\Query\Builder;

class AddressReadRepository implements IAddressReadRepository
{
    private Builder $builder;

    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

    public function findByStreet(string $street): array
    {
        $addresses = $this->builder->where("street", "=", $street)->get();

        return array_map(function ($address) {
            return AddressDto::fromArray((array)$address, [], []);
        }, $addresses->toArray());
    }
}
