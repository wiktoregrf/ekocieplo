<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Infrastructure\Mvc;

use Laravel\Lumen\Routing\Controller;
use Ekocieplo\Address\AddressesFacade;
use Ekocieplo\Shared\Response\Error;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AddressesController extends Controller
{
    private AddressesFacade $addressFacade;

    public function __construct(AddressesFacade $addressFacade)
    {
        $this->addressFacade = $addressFacade;
    }

    public function address(Request $request)
    {
        $arrayRequest = $request->all();
        try {
            Validator::make($arrayRequest, [
                'street' => 'required',
                'building_number' => 'required',
            ])->validate();
        } catch (ValidationException $ex) {
            return response(Error::handleBadRequest($ex->errors()), Response::HTTP_BAD_REQUEST);
        }

        $address = $this->addressFacade->getAddress($arrayRequest["street"], $arrayRequest["building_number"]);
        if ($address === null) {
            return response(Error::handleNotFound([
                "message" => sprintf("Address `%s` not found", $arrayRequest["street"])
            ]), Response::HTTP_NOT_FOUND);
        }

        return response($address);
    }
}
