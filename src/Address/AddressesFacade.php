<?php
declare(strict_types=1);

namespace Ekocieplo\Address;

use Ekocieplo\Address\Read\AddressReadService;
use Ekocieplo\Address\Read\Dto\AddressDto;

class AddressesFacade
{
    private AddressReadService $readService;

    public function __construct(AddressReadService $readService)
    {
        $this->readService = $readService;
    }

    public function getAddress(string $street, string $buildingNumber): ?AddressDto
    {
        return $this->readService->getAddressByStreetAndBuildingNumber($street, $buildingNumber);
    }
}
