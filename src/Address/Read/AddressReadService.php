<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Read;

use Ekocieplo\Address\Read\Dto\AddressDto;

class AddressReadService
{
    private IAddressReadRepository $addressReadRepository;
    private ICurrentHeatingAddressReadRepository $currentHeatingReadRepository;
    private IPlannedHeatingAddressReadRepository $plannedHeatingReadRepository;

    public function __construct(
        IAddressReadRepository $addressReadRepository,
        ICurrentHeatingAddressReadRepository $currentHeatingReadRepository,
        IPlannedHeatingAddressReadRepository $plannedHeatingReadRepository
    )
    {
        $this->addressReadRepository = $addressReadRepository;
        $this->currentHeatingReadRepository = $currentHeatingReadRepository;
        $this->plannedHeatingReadRepository = $plannedHeatingReadRepository;
    }

    public function getAddressByStreetAndBuildingNumber(string $street, string $buildingNumber): ?AddressDto
    {
        $addresses = $this->addressReadRepository->findByStreet($street);
        if(empty($addresses)) {
            return null;
        }

        $foundedRealEstate = array_values(array_filter($addresses, function (AddressDto $address) use ($buildingNumber) {
            return $address->getBuildingNumber() === $buildingNumber;
        }));
        if (!empty($foundedRealEstate)) {
            /** @var AddressDto $foundedRealEstate */
            $foundedRealEstate = $foundedRealEstate[0];
            $foundedRealEstate = $foundedRealEstate->withHeating($this->currentHeatingReadRepository->findByAddressId($foundedRealEstate->getId()));
            return $foundedRealEstate->withPlannedHeating($this->plannedHeatingReadRepository->findByAddressId($foundedRealEstate->getId()));
        }

        list($currentHeating, $plannedHeating) = $this->getAllHeatingAvailableOnTheStreet($addresses);
        $noRealEstate = array_filter($addresses, function (AddressDto $address) {
            return $address->getBuildingNumber() === null;
        })[0];
        $noRealEstate = $noRealEstate->withHeating($currentHeating);

        return $noRealEstate->withPlannedHeating($plannedHeating);
    }

    /**
     * @param AddressDto[] $addresses
     * @return array
     */
    private function getAllHeatingAvailableOnTheStreet(array $addresses): array
    {
        $currentHeatingSet = [];
        $plannedHeatingSet = [];
        $noRealEstate = null;

        foreach ($addresses as $address) {
            $currentHeatingSet = array_merge($currentHeatingSet, $this->currentHeatingReadRepository->findByAddressId($address->getId()));
            $plannedHeatingSet = array_merge($plannedHeatingSet, $this->plannedHeatingReadRepository->findByAddressId($address->getId()));
        }

        return [$currentHeatingSet, $plannedHeatingSet];
    }
}
