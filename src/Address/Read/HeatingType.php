<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Read;

final class HeatingType
{
    const GAS_NETWORK = 1;
    const GAS_TANK = 2;
    const ELECTRICITY = 3;
    const MSC = 4;
    const BIOMASS = 5;
}
