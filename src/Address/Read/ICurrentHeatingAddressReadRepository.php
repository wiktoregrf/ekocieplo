<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Read;

use Ekocieplo\Address\Read\Dto\HeatingDto;

interface ICurrentHeatingAddressReadRepository
{
    /**
     * @param string $addressId
     * @return HeatingDto[]
     */
    public function findByAddressId(string $addressId): array;
}
