<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Read;

use Illuminate\Support\ServiceProvider;

class ReadProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(AddressReadService::class, function () {
            return new AddressReadService(
                $this->app->get(IAddressReadRepository::class),
                $this->app->get(ICurrentHeatingAddressReadRepository::class),
                $this->app->get(IPlannedHeatingAddressReadRepository::class),
            );
        });
    }
}
