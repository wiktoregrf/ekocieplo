<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Read;

use Ekocieplo\Address\Read\Dto\AddressDto;

interface IAddressReadRepository
{
    /**
     * @param string $street
     * @return AddressDto[]
     */
    public function findByStreet(string $street): array;
}
