<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Read\Dto;

class AddressDto implements \JsonSerializable
{
    private string $id;

    private string $street;

    private ?string $buildingNumber;

    /** @var HeatingDto[] */
    private array $heating;

    /** @var PlannedHeatingDto[] */
    private array $plannedHeating;

    public function __construct(string $id, string $street, ?string $buildingNumber)
    {
        $this->id = $id;
        $this->street = $street;
        $this->buildingNumber = $buildingNumber;
        $this->heating = [];
        $this->plannedHeating = [];
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param HeatingDto[] $heating
     * @return $this
     */
    public function withHeating(array $heating): self
    {
        $new = clone $this;
        $new->heating = $heating;

        return $new;
    }

    /**
     * @param PlannedHeatingDto[] $plannedHeating
     * @return $this
     */
    public function withPlannedHeating(array $plannedHeating): self
    {
        $new = clone $this;
        $new->plannedHeating = $plannedHeating;

        return $new;
    }

    /** @return HeatingDto[] */
    public function getHeating(): array
    {
        return $this->heating;
    }

    public function getBuildingNumber(): ?string
    {
        return $this->buildingNumber;
    }

    public function getPlannedHeating(): array
    {
        return $this->plannedHeating;
    }

    public static function fromArray($data, $aHeating, $aPlannedHeating): AddressDto
    {
        $heating = array_map(function ($heating) {
            return HeatingDto::fromArray($heating);
        }, $aHeating);
        $plannedHeating = array_map(function ($heating) {
            return PlannedHeatingDto::fromArray($heating);
        }, $aPlannedHeating);

        $address = new AddressDto($data["id"], $data["street"], $data["building_number"]);
        $withHeating = $address->withHeating($heating);

        return $withHeating->withPlannedHeating($plannedHeating);
    }

    public function jsonSerialize()
    {
        $result = [
            "id" => $this->id,
            "street" => $this->street,
            "building_number" => $this->buildingNumber
        ];
        if (!empty($this->heating)) {
            $result["heating"] = $this->heating;
        }
        if (!empty($this->plannedHeating)) {
            $result["planned_heating"] = $this->plannedHeating;
        }
        return $result;
    }
}
