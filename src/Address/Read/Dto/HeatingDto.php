<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Read\Dto;

use Ekocieplo\Address\Infrastructure\Persistence\TableNames;
use Ramsey\Uuid\UuidInterface;

final class HeatingDto implements \JsonSerializable
{
    private string $id;

    private int $type;

    private int $externalInstallationPrice;

    private int $internalInstallationPriceS;

    private int $internalInstallationPriceM;

    private int $internalInstallationPriceL;

    private int $equipmentPrice;

    private int $operatingCostsS;

    private int $operatingCostsM;

    private int $operatingCostsL;

    public function __construct(
        string $id,
        int $type,
        int $externalInstallationPrice,
        int $internalInstallationPriceS,
        int $internalInstallationPriceM,
        int $internalInstallationPriceL,
        int $equipmentPrice,
        int $operatingCostsS,
        int $operatingCostsM,
        int $operatingCostsL
    )
    {
        $this->id = $id;
        $this->type = $type;
        $this->externalInstallationPrice = $externalInstallationPrice;
        $this->internalInstallationPriceS = $internalInstallationPriceS;
        $this->internalInstallationPriceM = $internalInstallationPriceM;
        $this->internalInstallationPriceL = $internalInstallationPriceL;
        $this->equipmentPrice = $equipmentPrice;
        $this->operatingCostsS = $operatingCostsS;
        $this->operatingCostsM = $operatingCostsM;
        $this->operatingCostsL = $operatingCostsL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getExternalInstallationPrice(): int
    {
        return $this->externalInstallationPrice;
    }

    public function getInternalInstallationPriceS(): int
    {
        return $this->internalInstallationPriceS;
    }

    public function getInternalInstallationPriceM(): int
    {
        return $this->internalInstallationPriceM;
    }

    public function getInternalInstallationPriceL(): int
    {
        return $this->internalInstallationPriceL;
    }

    public function getEquipmentPrice(): int
    {
        return $this->equipmentPrice;
    }

    public function getOperatingCostsS(): int
    {
        return $this->operatingCostsS;
    }

    public function getOperatingCostsM(): int
    {
        return $this->operatingCostsM;
    }

    public function getOperatingCostsL(): int
    {
        return $this->operatingCostsL;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data["id"],
            $data["type"],
            $data["external_installation_price"],
            $data["internal_installation_price_s"],
            $data["internal_installation_price_m"],
            $data["internal_installation_price_l"],
            $data["equipment_price"],
            $data["operating_costs_s"],
            $data["operating_costs_m"],
            $data["operating_costs_l"],
        );
    }

    public function calculateInvestmentCost(): int
    {
        return $this->externalInstallationPrice +
            $this->internalInstallationPriceS +
            $this->internalInstallationPriceM +
            $this->internalInstallationPriceL +
            $this->equipmentPrice;
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "type" => $this->type,
            "external_installation_price" => $this->externalInstallationPrice,
            "internal_installation_price_s" => $this->internalInstallationPriceS,
            "internal_installation_price_m" => $this->internalInstallationPriceM,
            "internal_installation_price_l" => $this->internalInstallationPriceL,
            "equipment_price" => $this->equipmentPrice,
            "operating_costs_s" => $this->operatingCostsS,
            "operating_costs_m" => $this->operatingCostsM,
            "operating_costs_l" => $this->operatingCostsL,
            "total_investment_cost" => $this->calculateInvestmentCost()
        ];
    }
}
