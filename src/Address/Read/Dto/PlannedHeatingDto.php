<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Read\Dto;

class PlannedHeatingDto implements \JsonSerializable
{
    private string $id;

    private int $type;

    private int $year;

    public function __construct(string $id, int $type, int $year)
    {
        $this->id = $id;
        $this->type = $type;
        $this->year = $year;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data["id"],
            $data["type"],
            $data["year"]
        );
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "type" => $this->type,
            "year" => $this->year,
        ];
    }
}
