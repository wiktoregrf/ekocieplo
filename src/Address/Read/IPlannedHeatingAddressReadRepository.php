<?php
declare(strict_types=1);

namespace Ekocieplo\Address\Read;

use Ekocieplo\Address\Read\Dto\PlannedHeatingDto;

interface IPlannedHeatingAddressReadRepository
{
    /**
     * @param string $addressId
     * @return PlannedHeatingDto[]
     */
    public function findByAddressId(string $addressId): array;
}
