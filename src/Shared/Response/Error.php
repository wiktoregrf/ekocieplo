<?php
declare(strict_types=1);

namespace Ekocieplo\Shared\Response;

class Error implements \JsonSerializable
{
    private int $code;
    private string $type;
    private array $details;

    public const TYPE_BAD_REQUEST = "BAD_REQUEST";
    public const TYPE_NOT_FOUND = "NOT_FOUND";
    public const TYPE_SERVER_ERROR = "SERVER_ERROR";
    public const TYPE_UNAUTHORIZED = "UNAUTHORIZED";
    public const TYPE_FORBIDDEN = "FORBIDDEN";

    public function __construct(
        int $code,
        string $type,
        array $details = []
    )
    {
        $this->code = $code;
        $this->type = $type;
        $this->details = $details;
    }

    public static function handleBadRequest(array $details): self
    {
        return new Error(400, self::TYPE_BAD_REQUEST, $details);
    }

    public static function handleNotFound(array $details): self
    {
        return new Error(404, self::TYPE_NOT_FOUND, $details);
    }

    public static function handleUnprocessableEntity(string $type, array $details): self
    {
        return new Error(422, $type, $details);
    }

    public static function handleServerError(array $details)
    {
        return new Error(500, self::TYPE_SERVER_ERROR, $details);
    }

    public static function handleUnauthorized(array $details)
    {
        return new Error(401, self::TYPE_UNAUTHORIZED, $details);
    }

    public static function handleForbidden(array $details)
    {
        return new Error(403, self::TYPE_UNAUTHORIZED, $details);
    }

    public function jsonSerialize()
    {
        return [
            "code" => $this->code,
            "type" => $this->type,
            "details" => $this->details
        ];
    }
}
