# Ekocieplo api

## Prerequisites

- [docker](https://www.docker.com/)
- [docker-compose](https://docs.docker.com/compose/)
- [make](https://www.gnu.org/software/make/)
- `sh`

### Configuration
1. Create `.env` file:`cp .env.example .env`

### Setup

#### Build & run application
1. `make build`


#### Run application
`make run`

#### Optional commands

 - ssh `make ssh`
 - database migration `make migrate`
 - database seed `make seed`
 - docker logs `make docker-logs`
 - fix cache permission `make fix-permissions`
