<?php

namespace Address\Read;

use Ekocieplo\Address\Read\AddressReadService;
use Ekocieplo\Address\Read\Dto\AddressDto;
use Ekocieplo\Address\Read\Dto\HeatingDto;
use Ekocieplo\Address\Read\Dto\PlannedHeatingDto;
use Ekocieplo\Address\Read\IAddressReadRepository;
use Ekocieplo\Address\Read\ICurrentHeatingAddressReadRepository;
use Ekocieplo\Address\Read\IPlannedHeatingAddressReadRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class AddressReadServiceTest extends TestCase
{
    /** @var IAddressReadRepository |MockObject */
    private $addressReadRepository;

    /** @var ICurrentHeatingAddressReadRepository |MockObject */
    private $currentHeatingReadRepository;

    /** @var IPlannedHeatingAddressReadRepository |MockObject */
    private $plannedHeatingReadRepository;

    /** @var AddressReadService */
    private $tested;

    protected function setUp(): void
    {
        $this->addressReadRepository = $this->createMock(IAddressReadRepository::class);
        $this->currentHeatingReadRepository = $this->createMock(ICurrentHeatingAddressReadRepository::class);
        $this->plannedHeatingReadRepository = $this->createMock(IPlannedHeatingAddressReadRepository::class);
        $this->tested = new AddressReadService(
            $this->addressReadRepository, $this->currentHeatingReadRepository, $this->plannedHeatingReadRepository
        );
        parent::setUp();
    }

    public function testGetAddressByStreetAndBuildingNumberForRealEstate()
    {
        //given
        $street = "abc";
        $buildingNumber = "2f";
        $heatingData = [
            "id"=> Uuid::uuid4()->toString(),
            "type"=> 0,
            "external_installation_price"=> 0,
            "internal_installation_price_s"=> 0,
            "internal_installation_price_m"=> 0,
            "internal_installation_price_l"=> 0,
            "equipment_price"=> 0,
            "operating_costs_s"=> 0,
            "operating_costs_m"=> 0,
            "operating_costs_l"=> 0,
            "total_investment_cost"=> 0
        ];
        $heatingDto = HeatingDto::fromArray($heatingData);
        $plannedHeatingData = [
            "id" => Uuid::uuid4()->toString(),
            "type" => 0,
            "heating_id" => 0,
            "year" => 2000,
        ];
        $plannedHeatingDto = PlannedHeatingDto::fromArray($plannedHeatingData);
        $address = AddressDto::fromArray([
            "id" => Uuid::uuid4()->toString(),
            "street" => $street,
            "building_number" => $buildingNumber,
        ], [$heatingData], [$plannedHeatingData]);

        $this->currentHeatingReadRepository
            ->method("findByAddressId")
            ->with($address->getId())
            ->willReturn([$heatingDto]);
        $this->plannedHeatingReadRepository
            ->method("findByAddressId")
            ->with($address->getId())
            ->willReturn([$plannedHeatingDto]);
        $this->addressReadRepository
            ->method("findByStreet")
            ->with($street)
            ->willReturn([$address]);

        // when
        $result = $this->tested->getAddressByStreetAndBuildingNumber($street, $buildingNumber);

        $this->assertEquals($result->getId(), $address->getId());
        $this->assertEquals($result->getStreet(), $address->getStreet());
        $this->assertEquals($result->getBuildingNumber(), $address->getBuildingNumber());
        $this->assertEquals($result->getPlannedHeating(), $address->getPlannedHeating());
        $this->assertEquals($result->getHeating(), $address->getHeating());
    }
}
