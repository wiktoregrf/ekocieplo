<?php
declare(strict_types=1);

use \Illuminate\Support\Facades\Schema;
use \Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddressTable extends Migration
{
    private string $tableName = "addresses";

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $blueprint) {
            $blueprint->uuid("id");$blueprint->primary("id");
            $blueprint->string("street", 64);
            $blueprint->string("building_number", 5)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
