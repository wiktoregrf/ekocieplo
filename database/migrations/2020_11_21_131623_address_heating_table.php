<?php
declare(strict_types=1);

use \Illuminate\Support\Facades\Schema;
use \Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddressHeatingTable extends Migration
{
    private string $tableName = "addresses_heating";

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $blueprint) {
            $blueprint->uuid("id");$blueprint->primary("id");
            $blueprint->uuid("address_id");
            $blueprint->uuid("heating_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
