<?php
declare(strict_types=1);

use \Illuminate\Support\Facades\Schema;
use \Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HeatingTable extends Migration
{
    private string $tableName = "heating";

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $blueprint) {
            $blueprint->uuid("id");$blueprint->primary("id");
            $blueprint->tinyInteger("type");
            $blueprint->mediumInteger("external_installation_price");
            $blueprint->mediumInteger("internal_installation_price_s");
            $blueprint->mediumInteger("internal_installation_price_m");
            $blueprint->mediumInteger("internal_installation_price_l");
            $blueprint->mediumInteger("operating_costs_s");
            $blueprint->mediumInteger("operating_costs_m");
            $blueprint->mediumInteger("operating_costs_l");
            $blueprint->mediumInteger("equipment_price");
        });
    }

    public function down()
    {
        Schema::drop($this->tableName);
    }
}
