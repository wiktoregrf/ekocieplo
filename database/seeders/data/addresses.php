<?php
return array (
    0 =>
        array (
            'id' => 'da245645-bf64-44c3-a3bd-3b41618f1e93',
            'street' => 'Dojazd',
            'building_number' => NULL,
        ),
    1 =>
        array (
            'id' => '67568771-f9bb-4fce-847b-13a784e676d3',
            'street' => 'Agrestowa',
            'building_number' => NULL,
        ),
    2 =>
        array (
            'id' => '26860bc9-5493-44a4-9602-327d1020c7db',
            'street' => 'Bałtycka',
            'building_number' => NULL,
        ),
    3 =>
        array (
            'id' => 'c8e64122-7da0-4271-a939-b09026a2b58f',
            'street' => 'Boczna',
            'building_number' => NULL,
        ),
    4 =>
        array (
            'id' => 'd8e52493-aa8d-400d-a6ab-f733247a54ae',
            'street' => 'Braterska',
            'building_number' => NULL,
        ),
    5 =>
        array (
            'id' => '755a2b6a-d7c7-4eea-8e1a-91afee4cb9cf',
            'street' => 'Fryderyka Chopina',
            'building_number' => NULL,
        ),
    6 =>
        array (
            'id' => 'dab727b0-ba2e-4844-baa2-34c71133aa27',
            'street' => 'Hetmana Stefana Czarneckiego',
            'building_number' => NULL,
        ),
    7 =>
        array (
            'id' => '58ffb093-4c0a-4bf4-993b-a9ee1e63227b',
            'street' => 'Czeska',
            'building_number' => NULL,
        ),
    8 =>
        array (
            'id' => 'c1df7836-ec66-4acf-9a6f-f35777eb7768',
            'street' => 'Dobra',
            'building_number' => NULL,
        ),
    9 =>
        array (
            'id' => '9f53ed8b-593e-4dfd-b3d0-446828b8d32d',
            'street' => 'Janusza Teodora Dybowskiego',
            'building_number' => NULL,
        ),
    10 =>
        array (
            'id' => '464694de-cccf-4d75-a4a3-12223019f650',
            'street' => 'Getta Żydowskiego',
            'building_number' => NULL,
        ),
    11 =>
        array (
            'id' => 'ae4ad48b-c4ad-4279-819f-f79d9f641a0a',
            'street' => 'Główna',
            'building_number' => NULL,
        ),
    12 =>
        array (
            'id' => '89a48a68-958e-4e88-852a-d8cae8821e46',
            'street' => 'Graniczna',
            'building_number' => NULL,
        ),
    13 =>
        array (
            'id' => 'e9cf731d-34dc-4557-9727-e49c5b6fdeee',
            'street' => 'Hetmańska',
            'building_number' => NULL,
        ),
    14 =>
        array (
            'id' => '917178a4-0898-42fa-866e-2367ef5b799f',
            'street' => 'Władysława Jagiełły',
            'building_number' => NULL,
        ),
    15 =>
        array (
            'id' => '8df0fbf4-72aa-4310-9357-f4ff13809f92',
            'street' => 'Jedności',
            'building_number' => NULL,
        ),
    16 =>
        array (
            'id' => '493e9ba0-9e13-40c1-b9e7-152401d961d3',
            'street' => 'Kanałowa',
            'building_number' => NULL,
        ),
    17 =>
        array (
            'id' => 'c320611c-269e-4bf0-beba-ef4e30ae98e7',
            'street' => 'Jana Kazimierza',
            'building_number' => NULL,
        ),
    18 =>
        array (
            'id' => '1fd78283-776c-4373-acf0-307c78417428',
            'street' => 'Jana Kilińskiego',
            'building_number' => NULL,
        ),
    19 =>
        array (
            'id' => 'b771ed15-b5e3-461b-90c5-b42773e9666e',
            'street' => 'Klonowa',
            'building_number' => NULL,
        ),
    20 =>
        array (
            'id' => '4ee23705-15f0-4700-b7cb-e69bddcb60bd',
            'street' => 'Prof. dr. Tadeusza Kobusiewicza',
            'building_number' => NULL,
        ),
    21 =>
        array (
            'id' => 'd674fb5e-6034-471f-b508-75261055c323',
            'street' => 'Św. Maksymiliana Marii Kolbego',
            'building_number' => NULL,
        ),
    22 =>
        array (
            'id' => 'b1f50874-f80d-4b31-bb4f-ebe2e2f520d8',
            'street' => 'Marii Konopnickiej',
            'building_number' => NULL,
        ),

    23 =>
        array (
            'id' => 'aa130459-1268-4188-aba2-25e5e06e49c2',
            'street' => 'Kościelna',
            'building_number' => NULL,
        ),
    24 =>
        array (
            'id' => 'e371b7f0-8220-4927-8b54-926101678300',
            'street' => 'Pasaż dr. Jakuba Lemberga',
            'building_number' => NULL,
        ),
    25 =>
        array (
            'id' => '48aee629-215f-40f4-a2a1-3858a165006d',
            'street' => 'Łaska',
            'building_number' => NULL,
        ),
    26 =>
        array (
            'id' => '0f762e58-1afb-4115-8ca3-2a4c3f068b04',
            'street' => 'Łąkowa',
            'building_number' => NULL,
        ),
    27 =>
        array (
            'id' => 'f4076cb3-ebb1-4a85-94c9-9bf014f32ef7',
            'street' => 'Łódzka',
            'building_number' => NULL,
        ),
    28 =>
        array (
            'id' => '7188249f-4903-4b95-bf60-db0b51aef897',
            'street' => 'Młynarska',
            'building_number' => NULL,
        ),
    29 =>
        array (
            'id' => '699ae332-0ef4-4aed-93da-5557108fb625',
            'street' => 'Mostowa',
            'building_number' => NULL,
        ),
    30 =>
        array (
            'id' => 'e22c2323-6ff0-49ea-8561-2934fa1a08c0',
            'street' => 'Murarska',
            'building_number' => NULL,
        ),
    31 =>
        array (
            'id' => 'f128a7ff-e832-4a40-a5f1-2e4831629870',
            'street' => 'Nadziei',
            'building_number' => NULL,
        ),
    32 =>
        array (
            'id' => '8b238bb6-b19d-4d6f-8e59-c372af62f2f1',
            'street' => 'Gabriela Narutowicza',
            'building_number' => NULL,
        ),
    33 =>
        array (
            'id' => 'c2b228db-8004-41ba-a3f4-b195b9594cca',
            'street' => 'Narwiańska',
            'building_number' => NULL,
        ),
    34 =>
        array (
            'id' => 'f1eaed11-22bf-4f28-af56-78c569a2ef27',
            'street' => 'Notecka',
            'building_number' => NULL,
        ),
    35 =>
        array (
            'id' => '57484072-55e9-4e8e-a665-892930ad1517',
            'street' => 'Obrońców Westerplatte',
            'building_number' => NULL,
        ),
    36 =>
        array (
            'id' => '1f0c5e50-aff8-47fd-80a3-5eacabaefd21',
            'street' => 'Odrzańska',
            'building_number' => NULL,
        ),
    37 =>
        array (
            'id' => '950a141a-df41-4459-9ad8-632760b37f9b',
            'street' => 'Opiesińska',
            'building_number' => NULL,
        ),
    38 =>
        array (
            'id' => '67efab43-82e3-48c0-b551-e9f17d1056ea',
            'street' => 'Piaskowa',
            'building_number' => NULL,
        ),
    39 =>
        array (
            'id' => '367531b3-77b8-4707-8878-5da5486b4dea',
            'street' => 'Longinusa Podbipięty',
            'building_number' => NULL,
        ),
    40 =>
        array (
            'id' => '95c84fc7-5ded-418e-8163-c64379af0f93',
            'street' => 'Podmiejska',
            'building_number' => NULL,
        ),
    41 =>
        array (
            'id' => '5a0cb4d3-ee05-4caa-97ba-9764774336ab',
            'street' => 'Pokoju',
            'building_number' => NULL,
        ),
    42 =>
        array (
            'id' => 'ed4e37a8-b7bb-4742-bc0c-e389dff3da66',
            'street' => 'Polna',
            'building_number' => NULL,
        ),
    43 =>
        array (
            'id' => '3d52efb1-913c-4fb1-9c82-9393379fffc4',
            'street' => 'Południowa',
            'building_number' => NULL,
        ),
    44 =>
        array (
            'id' => 'cc6756c3-3183-47ba-8797-43e9a3ff51e4',
            'street' => 'Haliny Poświatowskiej',
            'building_number' => NULL,
        ),
    45 =>
        array (
            'id' => 'dfa44980-78b3-4c07-9643-9e9104070480',
            'street' => 'Pasaż Powstańców Śląskich',
            'building_number' => NULL,
        ),
    46 =>
        array (
            'id' => 'a7574146-ec01-4a24-b9ca-e1e791b5656c',
            'street' => 'Prosta',
            'building_number' => NULL,
        ),
    47 =>
        array (
            'id' => '32354c5a-9f9d-4b5e-9c7f-3e95c63998d0',
            'street' => 'Radosna',
            'building_number' => NULL,
        ),
    48 =>
        array (
            'id' => '55ca3097-8054-495c-ae9d-cd6b7fb0b0c5',
            'street' => 'Mikołaja Reja',
            'building_number' => NULL,
        ),
    49 =>
        array (
            'id' => 'bd744210-91b6-4fd7-8831-bfcdd7c12341',
            'street' => 'Władysława Reymonta',
            'building_number' => NULL,
        ),
    50 =>
        array (
            'id' => 'b97cac8b-6f34-4a1e-94a1-36dd2688c628',
            'street' => 'Rycerska',
            'building_number' => NULL,
        ),
    51 =>
        array (
            'id' => 'ab5c68f8-ba06-43d0-80c4-8bc6af98ee62',
            'street' => 'Serdeczna',
            'building_number' => NULL,
        ),
    52 =>
        array (
            'id' => '9e72e02e-abce-4071-8d3c-fb58cfba6abc',
            'street' => 'Henryka Sienkiewicza',
            'building_number' => NULL,
        ),
    53 =>
        array (
            'id' => '82310e20-729b-48e3-be7a-1d541ecfadb9',
            'street' => 'Sieradzka',
            'building_number' => NULL,
        ),
    54 =>
        array (
            'id' => 'fd5c6bdf-bd1d-474b-9675-353eb8dc3986',
            'street' => 'Gen. Władysława Sikorskiego',
            'building_number' => NULL,
        ),
    55 =>
        array (
            'id' => 'e90ae1d6-1c3e-4971-8004-71541eabd30b',
            'street' => 'Jana Skrzetuskiego',
            'building_number' => NULL,
        ),
    56 =>
        array (
            'id' => '4f55ccfb-866b-4f7e-b036-0e517c01c649',
            'street' => 'Jana III Sobieskiego',
            'building_number' => NULL,
        ),
    57 =>
        array (
            'id' => 'a42539f8-12ac-40b9-82c5-c59b5e7f663f',
            'street' => 'Sokoła',
            'building_number' => NULL,
        ),
    58 =>
        array (
            'id' => 'dee497d5-278a-4a61-b989-50330c0eae6e',
            'street' => 'Andrzeja Struga',
            'building_number' => NULL,
        ),
    59 =>
        array (
            'id' => '29004e47-20da-45f3-a3df-88102c03634b',
            'street' => 'Strzelecka',
            'building_number' => NULL,
        ),
    60 =>
        array (
            'id' => '237fa61a-14ed-4299-9d61-2515c35a5df5',
            'street' => 'Szadkowska',
            'building_number' => NULL,
        ),
    61 =>
        array (
            'id' => '490a2e40-b035-4da8-912b-7d0dc0831274',
            'street' => 'Jerzego Szaniawskiego',
            'building_number' => NULL,
        ),
    62 =>
        array (
            'id' => '761f38f2-eda8-4ad7-9bbe-fbc5adaad8b6',
            'street' => 'Śnieżna',
            'building_number' => NULL,
        ),
    63 =>
        array (
            'id' => '30c6d341-9d0a-424f-badb-23379bf7b9e4',
            'street' => 'Ks. Prof. Józefa Tischnera',
            'building_number' => NULL,
        ),
    64 =>
        array (
            'id' => 'c769f9d8-d6a3-484d-af26-9aea7500e17a',
            'street' => 'Topolowa',
            'building_number' => NULL,
        ),
    65 =>
        array (
            'id' => '9b7b033d-8a6b-429c-99d0-3d7bbbad7d97',
            'street' => 'Torfowa',
            'building_number' => NULL,
        ),
    66 =>
        array (
            'id' => '7158e98c-cdbc-47db-a8b6-7731b6b3757d',
            'street' => 'Tymieniecka',
            'building_number' => NULL,
        ),
    67 =>
        array (
            'id' => '36a77c1c-1afb-4969-8177-d450c7ddfad9',
            'street' => 'Wczasowa',
            'building_number' => NULL,
        ),
    68 =>
        array (
            'id' => '99103216-fab0-4a55-a745-d19c3cd8bfd4',
            'street' => 'Wiklinowa',
            'building_number' => NULL,
        ),
    69 =>
        array (
            'id' => '6474a7d0-c0e3-4adc-be99-2a7ecba132ee',
            'street' => 'Wilcza',
            'building_number' => NULL,
        ),
    70 =>
        array (
            'id' => 'f71324d4-8833-420d-ae96-c4578757c92a',
            'street' => 'Wodna',
            'building_number' => NULL,
        ),
    71 =>
        array (
            'id' => '95bcbb81-4c90-4660-807f-636620df2dab',
            'street' => 'Wschodnia',
            'building_number' => NULL,
        ),
    72 =>
        array (
            'id' => '642820f3-17d0-44e3-9801-32940081a438',
            'street' => 'Kardynała Stefana Wyszyńskiego',
            'building_number' => NULL,
        ),
    73 =>
        array (
            'id' => '734b177b-9efe-41ee-95fe-85f9dce3c1d6',
            'street' => 'Zachodnia',
            'building_number' => NULL,
        ),
    74 =>
        array (
            'id' => 'aca593ff-42e9-4df4-9fd3-6f628760e1bd',
            'street' => 'Zagłoby',
            'building_number' => NULL,
        ),
    75 =>
        array (
            'id' => '39ebc2d9-4667-49b9-b3ab-47558c9adabb',
            'street' => 'Zakopiańska',
            'building_number' => NULL,
        ),
    76 =>
        array (
            'id' => '4c8da302-f81a-4323-b3b7-17b70ed65958',
            'street' => 'Zduńska',
            'building_number' => NULL,
        ),
    77 =>
        array (
            'id' => '46c82b20-5e4c-4f94-a1f1-0e5147e713a8',
            'street' => 'Zgody',
            'building_number' => NULL,
        ),
    78 =>
        array (
            'id' => 'f04bbe83-113d-4d47-9514-963288937565',
            'street' => 'Zielonogórska',
            'building_number' => NULL,
        ),
    79 =>
        array (
            'id' => '3fb10d4d-5469-49df-b158-58d244d3694f',
            'street' => 'Złota',
            'building_number' => NULL,
        ),
    80 =>
        array (
            'id' => '163eb249-c508-4593-8eb6-d118a4b48099',
            'street' => 'Stefana Złotnickiego',
            'building_number' => NULL,
        ),
    81 =>
        array (
            'id' => '9f4674e1-1d04-4085-97e7-eb4afa882fcb',
            'street' => 'Stanisława Żółkiewskiego',
            'building_number' => NULL,
        ),
    82 =>
        array (
            'id' => '8b13e127-dd56-43ef-a5cf-621e7ab48f34',
            'street' => 'Żurawia',
            'building_number' => NULL,
        ),
    83 =>
        array (
            'id' => 'b5638908-5363-4b5d-9427-a978fb118736',
            'street' => 'Żytnia',
            'building_number' => NULL,
        ),
    168 =>
        array (
            'id' => 'b289d018-de63-4a39-a9ef-17d532c8d4c7',
            'street' => 'Azaliowa',
            'building_number' => NULL,
        ),
    169 =>
        array (
            'id' => 'c780297b-14df-440a-a726-d3645e0b6ffa',
            'street' => 'Borowa',
            'building_number' => NULL,
        ),
    170 =>
        array (
            'id' => '94920afc-0c23-4b64-b5eb-07cff49f3f14',
            'street' => 'Jarosława Dąbrowskiego',
            'building_number' => NULL,
        ),
    171 =>
        array (
            'id' => '167cc7c8-04cd-4855-9505-65c702315b5b',
            'street' => 'Grabowa',
            'building_number' => NULL,
        ),
    172 =>
        array (
            'id' => '944daf61-5845-44f1-acf6-4244306439dc',
            'street' => 'Jodłowa',
            'building_number' => NULL,
        ),
    173 =>
        array (
            'id' => '18a25f96-9782-4eb7-b226-5b32e7bc19c9',
            'street' => 'Juliusza',
            'building_number' => NULL,
        ),
    174 =>
        array (
            'id' => '87e0f7ff-ed60-49d2-adcd-1f9900ef7f43',
            'street' => 'Karsznicka',
            'building_number' => NULL,
        ),
    175 =>
        array (
            'id' => '2b75097e-2c2a-4b90-9190-cb6ad0fbb6bd',
            'street' => 'Klasztorna',
            'building_number' => NULL,
        ),
    177 =>
        array (
            'id' => 'b04665c2-9638-45ab-ba6e-a8eee6904d4f',
            'street' => 'Kręta',
            'building_number' => NULL,
        ),
    179 =>
        array (
            'id' => '8424607a-4dc7-47bc-8800-40d8575e4f95',
            'street' => 'Adama Mickiewicza',
            'building_number' => NULL,
        ),
    181 =>
        array (
            'id' => 'ed206c1e-fedb-4154-8e46-8c6dd89227b8',
            'street' => 'Obywatelska',
            'building_number' => NULL,
        ),
    182 =>
        array (
            'id' => '5a544f77-4da7-4806-856d-2abdffd3d35c',
            'street' => 'Stefana Okrzei',
            'building_number' => NULL,
        ),
    183 =>
        array (
            'id' => 'd8ee22d6-7a49-4379-93a6-e0336504aeae',
            'street' => 'Orzycka',
            'building_number' => NULL,
        ),
    184 =>
        array (
            'id' => 'b8e0d677-9b1e-4c90-a24c-4a8c8d35e892',
            'street' => 'Osmolińska',
            'building_number' => NULL,
        ),
    185 =>
        array (
            'id' => 'abe47ccc-8864-4406-8878-3e5dd57c85cc',
            'street' => 'Paprocka',
            'building_number' => NULL,
        ),
    186 =>
        array (
            'id' => 'ca7df17f-89ec-4157-bc2e-07e7b761cff6',
            'street' => 'Piłkarska',
            'building_number' => NULL,
        ),
    189 =>
        array (
            'id' => '8d5370f5-1582-4386-a528-20ea5fe427b4',
            'street' => 'Słowiańska',
            'building_number' => NULL,
        ),
    190 =>
        array (
            'id' => '52d11aee-9b41-4a58-9ece-cd977397d5c4',
            'street' => 'Spacerowa',
            'building_number' => NULL,
        ),
    191 =>
        array (
            'id' => '7a51e0c8-d6e5-4b4a-be00-75711b942e47',
            'street' => 'Spółdzielcza',
            'building_number' => NULL,
        ),
    192 =>
        array (
            'id' => '4d6e16df-1127-4fce-a459-6dc0c16263b5',
            'street' => 'Srebrna',
            'building_number' => NULL,
        ),
    193 =>
        array (
            'id' => 'fa3a4a58-c3f4-4c4d-a310-e98956ac996b',
            'street' => 'Stanisława Staszica',
            'building_number' => NULL,
        ),
    194 =>
        array (
            'id' => '177bfd8c-fa9b-444f-bc84-e91ebc5ff0e9',
            'street' => 'Świerkowa',
            'building_number' => NULL,
        ),
    195 =>
        array (
            'id' => '041356c5-a4e3-4db6-8975-96cd88ccc8fa',
            'street' => 'Targowa',
            'building_number' => NULL,
        ),
    196 =>
        array (
            'id' => 'e3bf95fb-af4f-49ac-862f-566bb44da3a6',
            'street' => 'Tkacka',
            'building_number' => NULL,
        ),
    197 =>
        array (
            'id' => '2e1a1f3d-7b48-4521-a58a-34366a766d31',
            'street' => 'Tymienicka',
            'building_number' => NULL,
        ),
    198 =>
        array (
            'id' => 'd03d8d87-3a43-453d-bc20-9ac264cc9816',
            'street' => 'Warcka',
            'building_number' => NULL,
        ),
    200 =>
        array (
            'id' => '04f88a12-470f-419c-a143-abb44857e32f',
            'street' => 'Wspólna',
            'building_number' => NULL,
        ),
    201 =>
        array (
            'id' => '2e6b2f99-ccd1-46f4-b213-1624a2ac24fe',
            'street' => 'Zielona',
            'building_number' => NULL,
        ),
    203 =>
        array (
            'id' => 'ba434d08-18fe-43a6-8351-01d7ec89368e',
            'street' => 'Stefana Żeromskiego',
            'building_number' => NULL,
        ),
    205 =>
        array (
            'id' => 'b8fe86f3-c0f2-4e84-b8b3-c65459022663',
            'street' => 'Krzysztofa Kamila Baczyńskiego',
            'building_number' => NULL,
        ),
    206 =>
        array (
            'id' => '0a2635bc-c0f2-44cc-ace6-a86f33285699',
            'street' => 'Wojciecha Bogusławskiego',
            'building_number' => NULL,
        ),
    207 =>
        array (
            'id' => '6a595bbe-50d4-4f7a-ab54-a98d627bb4cc',
            'street' => 'Józefa Chełmońskiego',
            'building_number' => NULL,
        ),
    209 =>
        array (
            'id' => '514e6866-9e59-4fa6-b813-c57f441f315a',
            'street' => 'Cicha',
            'building_number' => NULL,
        ),
    211 =>
        array (
            'id' => 'd3515220-40f4-4ef2-be5c-2d7721784692',
            'street' => 'Długa',
            'building_number' => NULL,
        ),
    212 =>
        array (
            'id' => '128c13ff-b3d3-421a-b5d9-393c6945f1ef',
            'street' => 'Dolna',
            'building_number' => NULL,
        ),
    214 =>
        array (
            'id' => '5fffd86f-793b-4d9e-a7bd-112e4c676032',
            'street' => 'Aleksandra Gierymskiego',
            'building_number' => NULL,
        ),
    215 =>
        array (
            'id' => '721fe50c-b977-4d8a-a657-0648cd0376a0',
            'street' => 'Inżynierska',
            'building_number' => NULL,
        ),
    216 =>
        array (
            'id' => '9dfea73b-95f8-444e-84a2-eadc4d2645bd',
            'street' => 'Irysowa',
            'building_number' => NULL,
        ),
    217 =>
        array (
            'id' => 'c5c887f3-d3f5-470c-b82b-6ce5583fe2cb',
            'street' => 'Jarosława Iwaszkiewicza',
            'building_number' => NULL,
        ),
    218 =>
        array (
            'id' => '42c5352f-31e0-4f7e-a399-ce6c0c7875c3',
            'street' => 'Stefana Jaracza',
            'building_number' => NULL,
        ),
    219 =>
        array (
            'id' => '1f117cfc-4323-4080-966c-0582e4207ddc',
            'street' => 'Jasna',
            'building_number' => NULL,
        ),
    222 =>
        array (
            'id' => 'cb41cd9f-2d0f-4fc7-b7bb-fc9749a8c50a',
            'street' => 'Komisji Edukacji Narodowej',
            'building_number' => NULL,
        ),
    224 =>
        array (
            'id' => '37087b4c-35de-4870-94dc-bb7696bcf724',
            'street' => 'Prof. dr Tadeusza Kobusiewicza',
            'building_number' => NULL,
        ),
    225 =>
        array (
            'id' => '9bbfc8fd-716b-46d5-90e9-bfcb7e6ae436',
            'street' => 'Konwaliowa',
            'building_number' => NULL,
        ),
    226 =>
        array (
            'id' => 'a176626d-2be8-4d4b-aab4-d9a48bc405a3',
            'street' => 'Juliusza Kossaka',
            'building_number' => NULL,
        ),
    228 =>
        array (
            'id' => '915a171b-4bb5-401f-a3fd-f70c15e2f943',
            'street' => 'Aleje Tadeusza Kościuszki',
            'building_number' => NULL,
        ),
    230 =>
        array (
            'id' => '628bfd6c-d13e-44b4-ad89-2339630088da',
            'street' => 'Królewska',
            'building_number' => NULL,
        ),
    231 =>
        array (
            'id' => '855791a7-7dfa-4e86-932e-d9975a52e3a1',
            'street' => 'Kwiatowa',
            'building_number' => NULL,
        ),
    232 =>
        array (
            'id' => '852376ae-aa09-4c96-93c1-102bc4b52e90',
            'street' => 'Lawendowa',
            'building_number' => NULL,
        ),
    235 =>
        array (
            'id' => 'c62b29e4-f532-4f05-bbf7-6f0fc0ae97d5',
            'street' => 'Jacka Malczewskiego',
            'building_number' => NULL,
        ),
    236 =>
        array (
            'id' => 'dfaa97a4-f484-4e12-ad73-c045a75bc381',
            'street' => 'Miła',
            'building_number' => NULL,
        ),
    237 =>
        array (
            'id' => '95645c71-0fc7-48e6-b15d-1cece023aa67',
            'street' => 'Heleny Modrzejewskiej',
            'building_number' => NULL,
        ),
    240 =>
        array (
            'id' => '6e3c208a-df41-4ba6-b42e-92d19ce70ccf',
            'street' => 'Ogrodowa',
            'building_number' => NULL,
        ),
    243 =>
        array (
            'id' => '18fa625d-f974-4cad-936b-a5a5dff0857b',
            'street' => 'Ignacego Jana Paderewskiego',
            'building_number' => NULL,
        ),
    244 =>
        array (
            'id' => '2c5b2866-f1e7-476f-bb52-461fc5f8c4a2',
            'street' => 'Parkowa',
            'building_number' => NULL,
        ),
    245 =>
        array (
            'id' => '7f307a1d-ecf0-4dfa-b0e8-36c783e7f27e',
            'street' => 'Plac Krakowski',
            'building_number' => NULL,
        ),
    246 =>
        array (
            'id' => '9ad4c4f6-9981-40d7-a0f8-68e6a0de45cb',
            'street' => 'Plac Wolności',
            'building_number' => NULL,
        ),
    247 =>
        array (
            'id' => '82c5a513-4d54-446a-9b60-30832948d666',
            'street' => 'Pomorska',
            'building_number' => NULL,
        ),
    248 =>
        array (
            'id' => '54b273e3-e597-4d26-bce4-65cb9c95f6f6',
            'street' => 'Przejazd',
            'building_number' => NULL,
        ),
    249 =>
        array (
            'id' => '83c13602-c816-41d2-b259-38020420e80e',
            'street' => 'Różana',
            'building_number' => NULL,
        ),
    251 =>
        array (
            'id' => '9f500218-e611-4ff3-8f27-25dd2a62eed1',
            'street' => 'Marii Skłodowskiej-Curie',
            'building_number' => NULL,
        ),
    253 =>
        array (
            'id' => '06ee768c-78d2-44e3-bb13-ca73c8644284',
            'street' => 'Juliusza Słowackiego',
            'building_number' => NULL,
        ),
    256 =>
        array (
            'id' => '69421b32-76d7-4a52-bff7-a5573da312f6',
            'street' => 'Ludwika Solskiego',
            'building_number' => NULL,
        ),
    259 =>
        array (
            'id' => 'd3b4183a-36aa-4c49-b68b-5632900a3236',
            'street' => 'Leopolda Staffa',
            'building_number' => NULL,
        ),
    261 =>
        array (
            'id' => 'bcff7a31-aa4e-4006-9aa6-21e43b7af062',
            'street' => 'Szkolna',
            'building_number' => NULL,
        ),
    262 =>
        array (
            'id' => '37a100e2-cc04-42a4-9ecf-d66e87fed197',
            'street' => 'Szpitalna',
            'building_number' => NULL,
        ),
    263 =>
        array (
            'id' => 'a2900f8b-7e6d-4f07-adc0-f10e77cd007d',
            'street' => 'Karola Szymanowskiego',
            'building_number' => NULL,
        ),
    264 =>
        array (
            'id' => 'af8d3417-3163-45fb-9a3d-6cf819ce54ba',
            'street' => 'Juliusza Tuwima',
            'building_number' => NULL,
        ),
    265 =>
        array (
            'id' => '4704cc6d-f35a-47bc-b223-93e6a791ad72',
            'street' => 'Henryka Wieniawskiego',
            'building_number' => NULL,
        ),
    266 =>
        array (
            'id' => 'b0ef246c-fd69-416a-84a1-1fbcf72db46b',
            'street' => 'Wierzbowa',
            'building_number' => NULL,
        ),
    267 =>
        array (
            'id' => '65aa3ea8-452f-4d38-9e8e-fd8396d6c3ad',
            'street' => 'Wileńska',
            'building_number' => NULL,
        ),
    268 =>
        array (
            'id' => 'ae91915f-0442-4551-9dcc-993a9c153249',
            'street' => 'Stanisława Ignacego Witkiewicza',
            'building_number' => NULL,
        ),
    270 =>
        array (
            'id' => 'af799268-5308-4e66-9da4-c06ebf050686',
            'street' => 'Wolska',
            'building_number' => NULL,
        ),
    273 =>
        array (
            'id' => 'aaaa9e55-01ee-4c44-8680-0d3a8b64bca7',
            'street' => 'Ludwika Zamenhofa',
            'building_number' => NULL,
        ),
    279 =>
        array (
            'id' => '5c763566-f657-455f-9d18-02449bf3b0ba',
            'street' => '1 Maja',
            'building_number' => NULL,
        ),
    280 =>
        array (
            'id' => '6dffa2a6-2b1e-45f1-94f2-b1f1e345fa9d',
            'street' => 'Krucza',
            'building_number' => NULL,
        ),
    282 =>
        array (
            'id' => '2020ff01-2b54-423f-91d6-e1a1b5b485ea',
            'street' => 'Wojska Polskiego',
            'building_number' => NULL,
        ),
    283 =>
        array (
            'id' => 'f341ef03-b73b-49bf-828c-66daf2b7a1e3',
            'street' => 'Plac Krakowski',
            'building_number' => '12',
        ),
);
