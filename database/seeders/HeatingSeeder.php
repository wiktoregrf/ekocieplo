<?php
declare(strict_types=1);

namespace Database\Seeders;

use Ekocieplo\Address\Read\HeatingType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HeatingSeeder extends Seeder
{
    const GAS_NETWORK_ID = "32d316a1-7a4e-4620-be17-8ee3603504d2";
    const GAS_TANK_ID = "9de53e7e-1599-4c05-b123-7c9cdfe69d3f";
    const ELECTRICITY_ID = "58c225ef-d6d5-41e3-9fe8-922197a63501";
    const BIOMASS_ID = "2c58ed47-1f55-4aba-a2a4-d6b15082936f";
    const MSC_ID = "6d155dc4-24da-426d-bbec-ba87d7431af2";

    public function run()
    {
        DB::table("heating")->insert([
            $this->gasNetwork(),
            $this->gasTank(),
            $this->biomass(),
            $this->electricity(),
            $this->msc()
        ]);
    }

    private function gasNetwork()
    {
        $externalInstallationCosts = [
            "project" => 1750,
            "geoService" => 1150,
            "cabinet" => 550,
            "connectionCollection" => 150,
            "connectionFee" => 2250,
            "materialsAndLabor" => 1500,
        ];

        return [
            "id" => self::GAS_NETWORK_ID,
            "type" => HeatingType::GAS_NETWORK,
            "external_installation_price" => array_sum(array_values($externalInstallationCosts)),
            "internal_installation_price_s" => 10000,
            "internal_installation_price_m" => 15000,
            "internal_installation_price_l" => 22000,
            "operating_costs_s" => 3250,
            "operating_costs_m" => 4150,
            "operating_costs_l" => 5900,
            "equipment_price" => 9000
        ];
    }

    private function gasTank()
    {
        $externalInstallationCosts = [
            "project" => 1750,
            "gasTank" => 7750,
            "materials" => 500,
        ];

        return [
            "id" => self::GAS_TANK_ID,
            "type" => HeatingType::GAS_TANK,
            "external_installation_price" => array_sum(array_values($externalInstallationCosts)),
            "internal_installation_price_s" => 12000,
            "internal_installation_price_m" => 17000,
            "internal_installation_price_l" => 20000,
            "operating_costs_s" => 3250,
            "operating_costs_m" => 5000,
            "operating_costs_l" => 8500,
            "equipment_price" => 6000
        ];
    }

    private function biomass()
    {
        return [
            "id" => self::BIOMASS_ID,
            "type" => HeatingType::BIOMASS,
            "external_installation_price" => 0,
            "internal_installation_price_s" => 14000,
            "internal_installation_price_m" => 16000,
            "internal_installation_price_l" => 20000,
            "operating_costs_s" => 3250,
            "operating_costs_m" => 4500,
            "operating_costs_l" => 6500,
            "equipment_price" => 10250
        ];
    }

    private function electricity()
    {
        return [
            "id" => self::ELECTRICITY_ID,
            "type" => HeatingType::ELECTRICITY,
            "external_installation_price" => 0,
            "internal_installation_price_s" => 10000,
            "internal_installation_price_m" => 12000,
            "internal_installation_price_l" => 15500,
            "operating_costs_s" => 5500,
            "operating_costs_m" => 8000,
            "operating_costs_l" => 12500,
            "equipment_price" => 8250
        ];
    }

    private function msc()
    {
        return [
            "id" => self::MSC_ID,
            "type" => HeatingType::MSC,
            "external_installation_price" => 0,
            "internal_installation_price_s" => 15500,
            "internal_installation_price_m" => 15500,
            "internal_installation_price_l" => 15500,
            "operating_costs_s" => 4250,
            "operating_costs_m" => 5250,
            "operating_costs_l" => 10500,
            "equipment_price" => 15500
        ];
    }
}
