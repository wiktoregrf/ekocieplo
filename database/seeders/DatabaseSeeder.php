<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(AddressSeeder::class);
        $this->call(AddressHeatingSeeder::class);
        $this->call(HeatingSeeder::class);
        $this->call(AddressPlannedHeatingSeeder::class);
    }
}
