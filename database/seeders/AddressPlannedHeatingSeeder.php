<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class AddressPlannedHeatingSeeder extends Seeder
{
    public function run()
    {
        $addresses = require "data/addresses.php";
        $this->insertAddressesHeating($this->plannedMscInstallations(), $addresses,HeatingSeeder::MSC_ID);
        $this->insertAddressesHeating($this->plannedGasInstallation(), $addresses, HeatingSeeder::GAS_NETWORK_ID);
    }

    private function insertAddressesHeating(array $streets, array $addresses, string $heatingID)
    {
        foreach ($addresses as $address) {
            $result = array_filter($streets, function ($street) use ($address) {
                return $street["street"] === $address["street"] && $street["building_number"] === $address["building_number"];
            });

            if (!empty($result)) {
                $year = array_values(current($result))[2];
                DB::table("addresses_planned_heating")->insert([
                    "id" => Uuid::uuid4(),
                    "address_id" => $address["id"],
                    "heating_id" => $heatingID,
                    "year" => $year
                ]);
            }
        }
    }

    private function plannedMscInstallations()
    {
        $addresses = [
            ["street" => "1 Maja", "building_number" => null, "year" => 2021],
            ["street" => "Krucza", "building_number" => null, "year" => 2021],
            ["street" => "Plac Krakowski", "building_number" => 12, "year" => 2021],
            ["street" => "Wodna", "building_number" => null, "year" => 2022],
            ["street" => "Wojska Polskiego", "building_number" => null, "year" => 2022],
        ];

        return array_map(function ($address) {
            return [
                "street" => trim(ucfirst($address["street"])),
                "building_number" => $address["building_number"],
                "year" => $address["year"]
            ];
        }, $addresses);
    }

    private function plannedGasInstallation()
    {
        $addresses = [
            ["street" => "Azaliowa", "building_number" => null, "year" => 2023],
            ["street" => "Borowa", "building_number" => null, "year" => 2023],
            ["street" => "Jarosława Dąbrowskiego", "building_number" => null, "year" => 2023],
            ["street" => "Grabowa", "building_number" => null, "year" => 2023],
            ["street" => "Jodłowa", "building_number" => null, "year" => 2023],
            ["street" => "Juliusza", "building_number" => null, "year" => 2023],
            ["street" => "Karsznicka", "building_number" => null, "year" => 2023],
            ["street" => "Klasztorna", "building_number" => null, "year" => 2023],
            ["street" => "Kościelna", "building_number" => null, "year" => 2023],
            ["street" => "Kręta", "building_number" => null, "year" => 2023],
            ["street" => "Łaska", "building_number" => null, "year" => 2023],
            ["street" => "1 Maja", "building_number" => null, "year" => 2023],
            ["street" => "Adama Mickiewicza", "building_number" => null, "year" => 2023],
            ["street" => "Mostowa", "building_number" => null, "year" => 2023],
            ["street" => "Obywatelska", "building_number" => null, "year" => 2023],
            ["street" => "Stefana Okrzei", "building_number" => null, "year" => 2023],
            ["street" => "Orzycka", "building_number" => null, "year" => 2023],
            ["street" => "Osmolińska", "building_number" => null, "year" => 2023],
            ["street" => "Paprocka", "building_number" => null, "year" => 2023],
            ["street" => "Piłkarska", "building_number" => null, "year" => 2023],
            ["street" => "Polna", "building_number" => null, "year" => 2023],
            ["street" => "Sieradzka", "building_number" => null, "year" => 2023],
            ["street" => "Słowiańska", "building_number" => null, "year" => 2023],
            ["street" => "Spacerowa", "building_number" => null, "year" => 2023],
            ["street" => "Spółdzielcza", "building_number" => null, "year" => 2023],
            ["street" => "Srebrna", "building_number" => null, "year" => 2023],
            ["street" => "Stanisława Staszica", "building_number" => null, "year" => 2023],
            ["street" => "Świerkowa", "building_number" => null, "year" => 2023],
            ["street" => "Targowa", "building_number" => null, "year" => 2023],
            ["street" => "Tkacka", "building_number" => null, "year" => 2023],
            ["street" => "Tymienicka", "building_number" => null, "year" => 2023],
            ["street" => "Warcka", "building_number" => null, "year" => 2023],
            ["street" => "Wiklinowa", "building_number" => null, "year" => 2023],
            ["street" => "Wspólna", "building_number" => null, "year" => 2023],
            ["street" => "Zielona", "building_number" => null, "year" => 2023],
            ["street" => "Zielonogórska", "building_number" => null, "year" => 2023],
            ["street" => "Stefana Żeromskiego", "building_number" => null, "year" => 2023],
        ];

        return array_map(function ($address) {
            return [
                "street" => trim(ucfirst($address["street"])),
                "building_number" => $address["building_number"],
                "year" => $address["year"]
            ];
        }, $addresses);
    }
}
