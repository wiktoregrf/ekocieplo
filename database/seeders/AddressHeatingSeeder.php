<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class AddressHeatingSeeder extends Seeder
{
    public function run()
    {
        $this->insertAddressesHeating($this->currentMscInstallations(), HeatingSeeder::MSC_ID);
        $this->insertAddressesHeating($this->currentGasInstallation(), HeatingSeeder::GAS_NETWORK_ID);
    }

    private function insertAddressesHeating(array $streetNames, string $heatingID)
    {
        $addresses = require "data/addresses.php";
        foreach ($streetNames as $streetName) {

            $result = array_filter($addresses, function ($address) use ($streetName) {
                return $address["street"] === $streetName;
            });

            if (!empty($result)) {
                $id = array_values(current($result))[0];
                DB::table("addresses_heating")->insert([
                    "id" => Uuid::uuid4(),
                    "address_id" => $id,
                    "heating_id" => $heatingID
                ]);
            }
        }
    }

    private function currentMscInstallations()
    {
        $addresses = [
            "Azaliowa",
            "Krzysztofa Kamila Baczyńskiego",
            "Wojciecha Bogusławskiego",
            "Józefa Chełmońskiego",
            "Fryderyka Chopina",
            "Cicha",
            "Jarosława Dąbrowskiego",
            "Długa",
            "Dolna",
            "Getta Żydowskiego",
            "Aleksandra Gierymskiego",
            "Inżynierska",
            "Irysowa",
            "Jarosława Iwaszkiewicza",
            "Stefana Jaracza",
            "Jasna",
            "Juliusza",
            "Jana Kazimierza",
            "Komisji Edukacji Narodowej",
            "Jana Kilińskiego",
            "prof. dr Tadeusza Kobusiewicza",
            "Konwaliowa",
            "Juliusza Kossaka",
            "Kościelna",
            "Aleje Tadeusza Kościuszki",
            "Kręta",
            "Królewska",
            "Kwiatowa",
            "Lawendowa",
            "Łaska",
            "Łódzka",
            "Jacka Malczewskiego",
            "Miła",
            "Heleny Modrzejewskiej",
            "Mostowa",
            "Obrońców Westerplatte",
            "Ogrodowa",
            "Opiesińska",
            "Osmolińska",
            "Ignacego Jana Paderewskiego",
            "Parkowa",
            "Plac Krakowski",
            "Plac Wolności",
            "Pomorska",
            "Przejazd",
            "Różana",
            "Sieradzka",
            "Marii Skłodowskiej-Curie",
            "Jana Skrzetuskiego",
            "Juliusza Słowackiego",
            "Słowiańska",
            "Jana III Sobieskiego",
            "Ludwika Solskiego",
            "Spacerowa",
            "Srebrna",
            "Leopolda Staffa",
            "Szadkowska",
            "Szkolna",
            "Szpitalna",
            "Karola Szymanowskiego",
            "Juliusza Tuwima",
            "Henryka Wieniawskiego",
            "Wierzbowa",
            "Wileńska",
            "Stanisława Ignacego Witkiewicza",
            "Wodna",
            "Wolska",
            "Zachodnia",
            "Zakopiańska",
            "Ludwika Zamenhofa",
            "Zielona",
            "Złota",
            "Stefana Złotnickiego",
            "Stefana Żeromskiego",
            "Żytnia"
        ];

        return array_map(function ($address) {
            return trim(ucfirst($address));
        }, $addresses);
    }

    private function currentGasInstallation()
    {
        $addresses = [
            "Dojazd",
            "Agrestowa",
            "Bałtycka",
            "Boczna",
            "Braterska",
            "Fryderyka Chopina",
            "Hetmana Stefana Czarneckiego",
            "Czeska",
            "Dobra",
            "Janusza Teodora Dybowskiego",
            "Getta Żydowskiego",
            "Główna",
            "Graniczna",
            "Hetmańska",
            "Władysława Jagiełły",
            "Jedności",
            "Kanałowa",
            "Jana Kazimierza",
            "Jana Kilińskiego",
            "Klonowa",
            "prof. dr. Tadeusza Kobusiewicza",
            "św. Maksymiliana Marii Kolbego",
            "Marii Konopnickiej",
            "Kościelna",
            "pasaż dr. Jakuba Lemberga",
            "Łaska",
            "Łąkowa",
            "Łódzka",
            "Młynarska",
            "Mostowa",
            "Murarska",
            "Nadziei",
            "Gabriela Narutowicza",
            "Narwiańska",
            "Notecka",
            "Obrońców Westerplatte",
            "Odrzańska",
            "Opiesińska",
            "Piaskowa",
            "Longinusa Podbipięty",
            "Podmiejska",
            "Pokoju",
            "Polna",
            "Południowa",
            "Haliny Poświatowskiej",
            "Pasaż Powstańców Śląskich",
            "Prosta",
            "Radosna",
            "Mikołaja Reja",
            "Władysława Reymonta",
            "Rycerska",
            "Serdeczna",
            "Henryka Sienkiewicza",
            "Sieradzka",
            "Gen. Władysława Sikorskiego",
            "Jana Skrzetuskiego",
            "Jana III Sobieskiego",
            "Sokoła",
            "Andrzeja Struga",
            "Strzelecka",
            "Szadkowska",
            "Jerzego Szaniawskiego",
            "Śnieżna",
            "ks. Prof. Józefa Tischnera",
            "Topolowa",
            "Torfowa",
            "Tymieniecka",
            "Wczasowa",
            "Wiklinowa",
            "Wilcza",
            "Wodna",
            "Wschodnia",
            "kardynała Stefana Wyszyńskiego",
            "Zachodnia",
            "Zagłoby",
            "Zakopiańska",
            "Zduńska",
            "Zgody",
            "Zielonogórska",
            "Złota",
            "Stefana Złotnickiego",
            "Stanisława Żółkiewskiego",
            "Żurawia",
            "Żytnia",
        ];

        return array_map(function ($address) {
            return trim(ucfirst($address));
        }, $addresses);
    }
}
